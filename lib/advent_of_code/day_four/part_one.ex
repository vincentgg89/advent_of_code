defmodule AdventOfCode.DayFour.PartOne do
  @moduledoc """
  Documentation for `DayFour.PartOne`.
  """

  import AdventOfCode.InputHelpers

  @mandatory_fields [
    "byr",
    "iyr",
    "eyr",
    "hgt",
    "hcl",
    "ecl",
    "pid"
  ]
  @doc """
  Count passwords with all mandatory fields

  You can find an input file example in:
  https://adventofcode.com/2020/day/4/input

  ## Examples

      iex> AdventOfCode.DayFour.PartOne.execute("input.txt")
      237
  """
  def execute(input_file) do
    passports =
      input_file
      |> read_input
      |> String.split("\n\n")

    Enum.reduce(passports, 0, fn passport, valid_passports ->
      if valid_passport?(passport), do: valid_passports + 1, else: valid_passports
    end)
  end

  defp valid_passport?(passport) do
    Enum.all?(@mandatory_fields, &String.contains?(passport, &1))
  end
end
