defmodule AdventOfCode.DayFour.PartTwo do
  @moduledoc """
  Documentation for `DayFour.PartTwo`.
  """

  import AdventOfCode.InputHelpers

  @mandatory_fields %{
    "byr" => ~r/^((19[2-9][0-9])|(200[0-2]))$/,
    "iyr" => ~r/^((201[0-9])|(2020))$/,
    "eyr" => ~r/^((202[0-9])|(2030))$/,
    "hgt" => ~r/^(1(([5-8][0-9])|(9[0-3]))cm)|(((59)|(6[0-9])|(7[0-6]))in)$/,
    "hcl" => ~r/^#[0-9a-f]{6}$/,
    "ecl" => ~r/^(amb|blu|brn|gry|grn|hzl|oth)$/,
    "pid" => ~r/^\d{9}$/
  }
  @doc """
  Count passwords with all mandatory fields

  You can find an input file example in:
  https://adventofcode.com/2020/day/4/input

  ## Examples

      iex> AdventOfCode.DayFour.PartTwo.execute("input.txt")
      237
  """
  def execute(input_file) do
    passports =
      input_file
      |> read_input
      |> String.split("\n\n")

    Enum.reduce(passports, 0, fn passport, valid_passports ->
      if valid_passport?(passport), do: valid_passports + 1, else: valid_passports
    end)
  end

  defp valid_passport?(passport) do
    Enum.all?(@mandatory_fields, fn {field, regex} ->
      String.contains?(passport, field) && valid_field_value?(passport, field, regex)
    end)
  end

  defp valid_field_value?(passport, field, regex) do
    value = get_value!(passport, field)
    Regex.match?(regex, value)
  end

  defp get_value!(passport, field) do
    "#{field}:(?<#{field}>\\S*)( |\\n)?"
    |> Regex.compile!()
    |> Regex.named_captures(passport)
    |> Map.fetch!(field)
  end
end
