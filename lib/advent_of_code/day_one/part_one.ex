defmodule AdventOfCode.DayOne.PartOne do
  @moduledoc """
  Documentation for `DayOne.PartOne`.
  """
  import AdventOfCode.InputHelpers

  defstruct current_num: nil, left_nums: [], sum_value: nil, found_num: nil

  @doc """
  Find the two numbers that sums value_to_find and return is multiplied value.

  You can find an input file example in:
  https://adventofcode.com/2020/day/1/input

  ## Examples

      iex> AdventOfCode.DayOne.PartOne.execute("input.txt", 2020)
      4036

      iex> AdventOfCode.DayOne.PartOne.execute("input.txt", 2020)
      :not_found

  """
  def execute(input_file, value_to_find) do
    numbers = read_input(:as_integers, input_file)
    struct = %AdventOfCode.DayOne.PartOne{left_nums: numbers, sum_value: value_to_find}
    find_pairs(struct)
  end

  defp find_pairs(%{left_nums: [], found_num: nil}), do: :not_found

  defp find_pairs(struct = %{left_nums: [next_num | left_nums], found_num: nil}) do
    struct = %{struct | current_num: next_num, left_nums: left_nums}

    Enum.reduce_while(left_nums, struct, fn
      num, acc_struct = %{found_num: nil} ->
        {:cont, evaluate_numbers(acc_struct, num)}

      _num, acc_struct ->
        {:halt, acc_struct}
    end)
    |> find_pairs()
  end

  defp find_pairs(%{current_num: current_num, found_num: found_num}), do: current_num * found_num

  defp evaluate_numbers(struct = %{current_num: current_num, sum_value: sum_value}, num) do
    if(current_num + num == sum_value) do
      %{struct | found_num: num}
    else
      struct
    end
  end
end
