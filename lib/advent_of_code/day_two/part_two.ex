defmodule AdventOfCode.DayTwo.PartTwo do
  @moduledoc """
  Documentation for `DayTwo.PartTwo`.
  """
  import AdventOfCode.InputHelpers

  @pass_regex ~r/(?<index1>\d{1,})-(?<index2>\d{1,}) (?<character>[a-zA-Z]): (?<pass>[a-zA-Z]*)/

  @doc """
  Find the number of valid passwords(Index based policy)

  You can find an input file example in:
  https://adventofcode.com/2020/day/2/input

  ## Examples

      iex> AdventOfCode.DayTwo.PartTwo.execute("input.txt")
      456
  """
  def execute(input_file) do
    passwords = read_input(:as_strings, input_file)
    Enum.reduce(passwords, 0, &evaluate_password/2)
  end

  defp evaluate_password(pass, valid) do
    params = Regex.named_captures(@pass_regex, pass)

    pass_tuple =
      params["pass"]
      |> String.codepoints()
      |> List.to_tuple()

    index1 = String.to_integer(params["index1"])
    index2 = String.to_integer(params["index2"])
    valid? = validate_password(pass_tuple, index1, index2, params["character"])

    if valid?, do: valid + 1, else: valid
  end

  defp validate_password(pass_tuple, index1, index2, char) do
    first_char = elem(pass_tuple, index1 - 1)
    second_char = elem(pass_tuple, index2 - 1)

    Enum.count([first_char, second_char], &(&1 == char)) == 1
  end
end
