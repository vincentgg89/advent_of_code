defmodule AdventOfCode.DayTwo.PartOne do
  @moduledoc """
  Documentation for `DayTwo.PartOne`.
  """
  import AdventOfCode.InputHelpers

  @pass_regex ~r/(?<min>\d{1,})-(?<max>\d{1,}) (?<character>[a-zA-Z]): (?<pass>[a-zA-Z]*)/

  @doc """
  Find the number of valid passwords(Count based policy)

  You can find an input file example in:
  https://adventofcode.com/2020/day/2/input

  ## Examples

      iex> AdventOfCode.DayTwo.PartOne.execute("input.txt")
      456
  """
  def execute(input_file) do
    passwords = read_input(:as_strings, input_file)
    Enum.reduce(passwords, 0, &evaluate_password/2)
  end

  defp evaluate_password(pass, valid) do
    params = Regex.named_captures(@pass_regex, pass)
    pass_list = String.codepoints(params["pass"])
    min = String.to_integer(params["min"])
    max = String.to_integer(params["max"])
    char_count = Enum.count(pass_list, &(&1 == params["character"]))

    if min <= char_count and char_count <= max, do: valid + 1, else: valid
  end
end
