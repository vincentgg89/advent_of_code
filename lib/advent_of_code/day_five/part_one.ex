defmodule AdventOfCode.DayFive.PartOne do
  @moduledoc """
  Documentation for `DayFive.PartOne`.
  """
  import AdventOfCode.InputHelpers

  @doc """
  Find the greatest boading pass ID

  You can find an input file example in:
  https://adventofcode.com/2020/day/5/input

  ## Examples

      iex> AdventOfCode.DayFive.PartOne.execute("input.txt", 128, 8)
      456
  """
  def execute(input_file, max_rows, max_columns) do
    passes = read_input(:as_strings, input_file)

    Enum.map(passes, fn pass ->
      row_steps = String.slice(pass, 0, 7)
      column_steps = String.slice(pass, 7, 3)
      calculate_seat(row_steps, column_steps, max_rows, max_columns)
    end)
    |> Enum.max()
  end

  defp calculate_seat(row_steps, column_steps, max_rows, max_columns) do
    row_steps = String.codepoints(row_steps)
    column_steps = String.codepoints(column_steps)
    row = process_step(row_steps, 0, max_rows - 1)
    column = process_step(column_steps, 0, max_columns - 1)
    row * 8 + column
  end

  defp process_step([step | []], min, _max) when step in ["F", "L"], do: min
  defp process_step([step | []], _min, max) when step in ["B", "R"], do: max

  defp process_step([step | other_steps], min, max) when step in ["F", "L"],
    do: process_step(other_steps, min, div(max + min, 2))

  defp process_step([step | other_steps], min, max) when step in ["B", "R"],
    do: process_step(other_steps, div(max + min, 2) + 1, max)
end
