defmodule AdventOfCode.DaySix.PartOne do
  @moduledoc """
  Documentation for `DaySix.PartOne`.
  """

  import AdventOfCode.InputHelpers

  @doc """
  Count passwords with all mandatory fields

  You can find an input file example in:
  https://adventofcode.com/2020/day/4/input

  ## Examples

      iex> AdventOfCode.DaySix.PartOne.execute("input.txt")
      237
  """
  def execute(input_file) do
    groups =
      input_file
      |> read_input
      |> String.split("\n\n")

    Enum.map(groups, fn group ->
      Regex.scan(~r/[a-z]/, group)
      |> Stream.uniq()
      |> Enum.count()
    end)
    |> Enum.sum()
  end
end
