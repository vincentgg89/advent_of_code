defmodule AdventOfCode.DaySix.PartTwo do
  @moduledoc """
  Documentation for `DaySix.PartOne`.
  """

  import AdventOfCode.InputHelpers

  @doc """
  Count passwords with all mandatory fields

  You can find an input file example in:
  https://adventofcode.com/2020/day/4/input

  ## Examples

      iex> AdventOfCode.DaySix.PartOne.execute("input.txt")
      237
  """
  def execute(input_file) do
    groups =
      input_file
      |> read_input
      |> String.split("\n\n")

    Enum.reduce(groups, 0, &process_group/2)
  end

  defp process_group([], sum), do: sum

  defp process_group(group, sum) do
    group
    |> String.split("\n")
    |> process_members()
    |> Kernel.+(sum)
  end

  defp process_members([member | members]) do
    answers = Regex.scan(~r/[a-z]/, member)
    process_members(members, MapSet.new(answers))
  end

  defp process_members([], prev_answers), do: MapSet.size(prev_answers)

  defp process_members([member | members], prev_answers) do
    answers =
      ~r/[a-z]/
      |> Regex.scan(member)
      |> MapSet.new()

    common = MapSet.intersection(answers, prev_answers)
    process_members(members, common)
  end
end
