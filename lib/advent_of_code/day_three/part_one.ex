defmodule AdventOfCode.DayThree.PartOne do
  @moduledoc """
  Documentation for `DayThree.PartOne`.
  """
  import AdventOfCode.InputHelpers

  defstruct pointer: 3, trees: 0, line_length: 0

  @doc """
  Count trees found in trayectory

  You can find an input file example in:
  https://adventofcode.com/2020/day/3/input

  ## Examples

      iex> AdventOfCode.DayThree.PartOne.execute("input.txt")
      237
  """
  def execute(input_file) do
    [first_line | lines] = read_input(:as_strings, input_file)

    %AdventOfCode.DayThree.PartOne{line_length: String.length(first_line)}
    |> calculate_trees(lines)
    |> Map.get(:trees)
  end

  defp calculate_trees(struct, lines) do
    Enum.reduce(lines, struct, fn line, struct_acc ->
      is_tree? =
        line
        |> String.codepoints()
        |> List.to_tuple()
        |> elem(struct_acc.pointer)
        |> point_is_tree?()

      new_pointer = update_pointer(struct_acc.pointer, struct_acc.line_length)

      if is_tree? do
        %{struct_acc | trees: struct_acc.trees + 1, pointer: new_pointer}
      else
        %{struct_acc | pointer: new_pointer}
      end
    end)
  end

  defp update_pointer(curr_pointer, line_length) when curr_pointer + 3 < line_length,
    do: curr_pointer + 3

  defp update_pointer(curr_pointer, line_length), do: curr_pointer + 3 - line_length

  defp point_is_tree?("."), do: false
  defp point_is_tree?("#"), do: true
end
