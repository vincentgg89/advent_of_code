defmodule AdventOfCode.DayThree.PartTwo do
  @moduledoc """
  Documentation for `DayThree.PartTwo`.
  """
  import AdventOfCode.InputHelpers

  defstruct right: 1, down: 1, right_pointer: 0, down_pointer: 0, trees: 0, line_pointer: 0

  @doc """
  Count trees found in trayectory(multiple movements)

  You can find an input file example in:
  https://adventofcode.com/2020/day/3/input

  ## Examples

      iex> AdventOfCode.DayThree.PartTwo.execute("input.txt", [%AdventOfCode.DayThree.PartTwo{right: 1, down: 2}, ...])
      237
  """
  def execute(input_file, movements) do
    [first_line | lines] = read_input(:as_strings, input_file)
    line_length = String.length(first_line)

    lines
    |> calculate_trees(movements, line_length)
    |> Enum.reduce(1, &(&1.trees * &2))
  end

  defp calculate_trees(lines, movements, line_length) do
    Enum.reduce(lines, movements, fn line, movements_acc ->
      process_movements(line, movements_acc, line_length)
    end)
  end

  defp process_movements(line, movements, line_length) do
    Enum.map(movements, fn movement ->
      rp = update_right_pointer(movement.right_pointer, movement.right, line_length)
      dp = movement.down_pointer + movement.down
      lp = movement.line_pointer + 1
      evaluate_line_movement(line, movement, rp, dp, lp)
    end)
  end

  defp evaluate_line_movement(line, movement, rp, dp, lp) when dp == lp do
    is_tree? =
      line
      |> String.codepoints()
      |> List.to_tuple()
      |> elem(rp)
      |> point_is_tree?()

    new_movement = %{movement | down_pointer: dp, right_pointer: rp, line_pointer: lp}
    if is_tree?, do: %{new_movement | trees: movement.trees + 1}, else: new_movement
  end

  defp evaluate_line_movement(_line, movement, _rp, _dp, lp), do: %{movement | line_pointer: lp}

  defp update_right_pointer(curr_pointer, right_steps, line_length)
       when curr_pointer + right_steps < line_length,
       do: curr_pointer + right_steps

  defp update_right_pointer(curr_pointer, right_steps, line_length),
    do: curr_pointer + right_steps - line_length

  defp point_is_tree?("."), do: false
  defp point_is_tree?("#"), do: true
end
