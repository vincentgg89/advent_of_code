defmodule AdventOfCode do
  @moduledoc """
  Documentation for `AdventOfCode`.
  """

  @doc """
  Hello world.

  ## Examples

      iex> AdventOfCode.hello()
      "advent of code 2020"

  """
  def hello do
    "advent of code 2020"
  end
end
