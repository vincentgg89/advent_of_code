defmodule DaySixTest do
  use ExUnit.Case
  doctest AdventOfCode

  test "Count group answers and sums all" do
    assert AdventOfCode.DaySix.PartOne.execute("test/inputs/day_six.txt") == 6726
  end

  test "Count common group answers and sums all" do
    assert AdventOfCode.DaySix.PartTwo.execute("test/inputs/day_six.txt") == 3316
  end
end
