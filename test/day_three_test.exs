defmodule DayThreeTest do
  use ExUnit.Case
  doctest AdventOfCode

  test "Count trees in trayectory(x3, y1)" do
    assert AdventOfCode.DayThree.PartOne.execute("test/inputs/day_three.txt") == 237
  end

  test "Count trees in trayectory(many movements)" do
    movements = [
      %AdventOfCode.DayThree.PartTwo{right: 1, down: 1},
      %AdventOfCode.DayThree.PartTwo{right: 3, down: 1},
      %AdventOfCode.DayThree.PartTwo{right: 5, down: 1},
      %AdventOfCode.DayThree.PartTwo{right: 7, down: 1},
      %AdventOfCode.DayThree.PartTwo{right: 1, down: 2}
    ]

    assert AdventOfCode.DayThree.PartTwo.execute("test/inputs/day_three.txt", movements) ==
             2_106_818_610
  end
end
