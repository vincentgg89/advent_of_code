defmodule DayTwoTest do
  use ExUnit.Case
  doctest AdventOfCode

  test "Count valid passwords(Count based policy)" do
    assert AdventOfCode.DayTwo.PartOne.execute("test/inputs/day_two.txt") == 469
  end

  test "Count valid passwords(Index based policy)" do
    assert AdventOfCode.DayTwo.PartTwo.execute("test/inputs/day_two.txt") == 267
  end
end
