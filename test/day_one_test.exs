defmodule DayOneTest do
  use ExUnit.Case
  doctest AdventOfCode

  test "find two numbers that sums 2020" do
    # Benchee.run(
    #   %{
    #   "day1.part1" => fn -> AdventOfCode.DayOne.PartOne.execute("test/inputs/day_one.txt", 2020) end
    #   }
    # )
    assert AdventOfCode.DayOne.PartOne.execute("test/inputs/day_one.txt", 2020) == 921_504
  end

  test "find three numbers that sums 2020" do
    # Benchee.run(
    #   %{
    #   "day1.part2" => fn -> AdventOfCode.DayOne.PartTwo.execute("test/inputs/day_one.txt", 2020) end
    #   }
    # )
    assert AdventOfCode.DayOne.PartTwo.execute("test/inputs/day_one.txt", 2020) == 195_700_142
  end
end
