defmodule DayFourTest do
  use ExUnit.Case
  doctest AdventOfCode

  test "Count valid passports" do
    assert AdventOfCode.DayFour.PartOne.execute("test/inputs/day_four.txt") == 237
  end

  test "Validate and Count valid passports" do
    assert AdventOfCode.DayFour.PartTwo.execute("test/inputs/day_four.txt") == 172
  end
end
