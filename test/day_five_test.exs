defmodule DayFiveTest do
  use ExUnit.Case
  doctest AdventOfCode

  test "Find max seatID" do
    # Benchee.run(
    #   %{
    #   "day5.part1" => fn -> AdventOfCode.DayFive.PartOne.execute("test/inputs/day_five.txt", 128, 8) end
    #   }
    # )
    assert AdventOfCode.DayFive.PartOne.execute("test/inputs/day_five.txt", 128, 8) == 832
  end

  test "Find missing seatID(My seat)" do
    # Benchee.run(
    #   %{
    #   "day5.part2" => fn -> AdventOfCode.DayFive.PartTwo.execute("test/inputs/day_five.txt", 128, 8) end
    #   }
    # )
    assert AdventOfCode.DayFive.PartTwo.execute("test/inputs/day_five.txt", 128, 8) == 517
  end
end
